<?php

$postData = $_POST;
if (is_null($postData) || empty($postData)) return;

$address = $postData['address'];
$roomType = $postData['room_type'];
$sleeps = $postData['sleeps'];
$bed = $postData['bed'];
$bath = $postData['bath'];

$content = file_get_contents('https://www.eliotandme.com/estimator?address=' . transformAddress($address) .
    '&bath=' . $bath . '&bed=' . $bed . '&room_type=' . $roomType . '&sleeps=' . $sleeps);

echo getRates($content);
return;

function transformAddress($address) {
    $addressArr = array_map('trim', explode(',', urlencode($address)));
    return implode('%2C+', $addressArr);
}

function getRates($content) {
    preg_match_all('/<h1 id="dailyRate">(.*?)<\/h1>/s', $content, $matchesDaily);

    if (isset($matchesDaily[1][0])) {
        preg_match_all('/<h1 id="weeklyRate">(.*?)<\/h1>/s', $content, $matchesWeekly);
        return '<div class="alert alert-success" role="alert"><p><strong>Καθημερινές:</strong> ' . $matchesDaily[1][0] . '</p><p><strong>Εβδομαδιαίες:</strong> ' . $matchesWeekly[1][0] . '</p></div>';
    }

    return '<div class="alert alert-warning" role="alert"><p>Δεν υπάρχουν διαθέσιμες τιμές για αυτή τη διεύθυνση.</p></div>';
}
